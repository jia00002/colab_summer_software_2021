# RUN python test.py to check your answer!

###
# CHALLENGE ONE
###
# Write a method which will multiply two numbers, without using the * operator:
# The method should return an integer
# WRITE YOUR CODE HERE.  Name your method `multiply`.


###
# CHALLENGE TWO
###
# Writ
# Write a method which returns:
#
#   * "Fizz" if the number is divisible by 3
#   * "Buzz" if the number is divisible by 5
#   * "FizzBuzz" if the number is divisible by 3 and 5
#   * Otherwise, return the number itself
#
# Remember that the % operator (modulo) is your friend.  It returns a zero if one
# number is divisible by another number.  In other words, 4 % 2 == 0.

# WRITE YOUR CODE HERE.  Name your method `fizzbuzz`.


